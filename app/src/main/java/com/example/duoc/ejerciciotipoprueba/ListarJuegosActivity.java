package com.example.duoc.ejerciciotipoprueba;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ListarJuegosActivity extends AppCompatActivity {

    private EditText txtListado;
    private Button btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_juegos);

        txtListado = (EditText) findViewById(R.id.txtListado);
        btnVolver  = (Button) findViewById(R.id.btnVolver);

        int contador = 1;
        for(Juegos aux : BD.getValues()){
            txtListado.append(contador + ") " + aux.toString());
            contador++;
        }


        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volver();
            }
        });
    }

    private void volver() {
        finish();
    }


}
