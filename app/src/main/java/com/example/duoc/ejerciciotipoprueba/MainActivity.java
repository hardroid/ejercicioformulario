package com.example.duoc.ejerciciotipoprueba;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

    private EditText txtNombreJuego, txtCategoriaJuego;
    private Button btnAgregar, btnVer;
    private CheckBox chkVigente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombreJuego = (EditText)findViewById(R.id.txtNombreJuego);
        txtCategoriaJuego = (EditText)findViewById(R.id.txtCategoriaJuego);
        chkVigente = (CheckBox)findViewById(R.id.chkVigente);
        btnAgregar = (Button)findViewById(R.id.btnAgregar);
        btnVer = (Button)findViewById(R.id.btnVer);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarJuegos();
            }
        });

        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verListado();
            }
        });
    }

    private void verListado() {
        Intent i = new Intent(this, ListarJuegosActivity.class);
        startActivity(i);
    }

    private void agregarJuegos() {
        Juegos aux = new Juegos();
        aux.setNombre(txtNombreJuego.getText().toString());
        aux.setCategoria(txtCategoriaJuego.getText().toString());
        aux.setVigente(chkVigente.isChecked());
        BD.agregarJuego(aux);
        txtNombreJuego.setText("");
        txtCategoriaJuego.setText("");
        chkVigente.setChecked(false);
    }
}
