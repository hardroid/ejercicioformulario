package com.example.duoc.ejerciciotipoprueba;

/**
 * Created by Duoc on 07-04-2016.
 */
public class Juegos {
    private String nombre;
    private String categoria;
    private boolean vigente;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public boolean isVigente() {
        return vigente;
    }

    public void setVigente(boolean vigente) {
        this.vigente = vigente;
    }

    @Override
    public String toString() {
        return  "Nombre Juego: " + nombre + "\n" +
                "Categoria Juego: " + categoria + "\n"+
                "Vigente: " + (isVigente()?"Si":"No") + "\n";
    }
}
